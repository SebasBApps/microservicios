package com.bety.store.customer.service;

import com.bety.store.customer.repository.entity.Customer;
import com.bety.store.customer.repository.entity.Region;

import java.util.List;

public interface CustomerService {

    List<Customer> findCustomerAll();
    List<Customer> findCustomersByRegion(Region region);

    Customer createCustomer(Customer customer);
    Customer updateCustomer(Customer customer);
    Customer deleteCustomer(Customer customer);
    Customer getCustomer(Long id);


}
