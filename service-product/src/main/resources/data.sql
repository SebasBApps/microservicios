INSERT INTO categories(id, name) VALUES(1, 'shoes');
INSERT INTO categories(id, name) VALUES(2, 'books');
INSERT INTO categories(id, name) VALUES(3, 'cellphones');

INSERT INTO products(id, name, description, price, stock, status, creation_Date, id_category)
VALUES(1, 'Lacoste Brand 1', 'Lacoste Brand 1 Description', 126.48, 100, 'Created', '2020-05-08', 1);

INSERT INTO products(id, name, description, price, stock, status, creation_Date, id_category)
VALUES(2, 'Adidas Brand 1', 'Adidas Brand 1 Description', 92.49, 150,  'Created', '2020-05-08', 1);

INSERT INTO products(id, name, description, price, stock, status, creation_Date, id_category)
VALUES(3, 'Bok Brand 1', 'Book Brand 1 Description', 26.48, 1000, 'Created', '2020-05-08', 2);