package com.bety.store.service.product.repository;

import com.bety.store.service.product.entity.Category;
import com.bety.store.service.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductRepository extends JpaRepository<Product, Long> {

    public List<Product> findByCategory(Category category);
}
