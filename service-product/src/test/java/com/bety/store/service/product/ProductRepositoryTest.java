package com.bety.store.service.product;

import com.bety.store.service.product.entity.Category;
import com.bety.store.service.product.entity.Product;
import com.bety.store.service.product.repository.IProductRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;

@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private IProductRepository iProductRepository;

    @Test
    public void whenFindByCategory_then_returnProductList(){
        Product product01 = Product.builder()
                .name("computer")
                .category(Category.builder().id(1l).build())
                .description("")
                .stock(Double.parseDouble("10"))
                .price(Double.parseDouble("1000.23"))
                .status("Created")
                .creationDate(new Date()).build();

        iProductRepository.save(product01);

        List<Product> productList = iProductRepository.findByCategory(Category.builder().id(1l).build());

        Assertions.assertThat(productList.size()).isEqualTo(3);
    }
}
