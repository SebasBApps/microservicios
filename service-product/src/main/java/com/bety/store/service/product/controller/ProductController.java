package com.bety.store.service.product.controller;

import com.bety.store.service.product.entity.Category;
import com.bety.store.service.product.entity.Product;
import com.bety.store.service.product.service.IProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    private final static Logger log = Logger.getLogger(ProductController.class.getName());
    @Autowired
    private IProductService productService;

    @GetMapping
    public ResponseEntity<List<Product>> getProducts(
            @RequestParam(name = "categoryId", required = false) Long categoryId) {

        List<Product> productList = new ArrayList<>();
        if (categoryId == null) {
            productList = productService.listAllProduct();
            if (productList.isEmpty())
                ResponseEntity.noContent().build();
        } else {
            productList = productService.findByCategory(Category.builder().id(categoryId).build());
            if (productList.isEmpty())
                ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(productList);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {
        Product product = productService.getProduct(id);

        return product == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(product);
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@Valid @RequestBody Product product, BindingResult result) {
        if (result.hasErrors())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, formatMessage(result));

        Product productCreated = productService.createProduct(product);
        return ResponseEntity.status(HttpStatus.CREATED).body(productCreated);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") Long id, @RequestBody Product product) {
        product.setId(id);
        Product productUpdated = productService.updateProduct(product);

        return productUpdated == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(productUpdated);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable("id") Long id) {
        Product product = productService.deleteProduct(id);
        return product == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(product);
    }

    @GetMapping(value = "/{id}/stock")
    public ResponseEntity<Product> updateStockProduct(@PathVariable("id") Long id,
                                                      @RequestParam(name = "qty", required = true) Double qty) {
        Product product = productService.updateStock(id, qty);
        return product == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(product);
    }

    /**
     * Return the message validation.
     * @param result
     * @return
     */
    private String formatMessage(BindingResult result) {
        List<Map<String, String>> messages = result.getFieldErrors().stream().map(
                err -> {
                    Map<String, String> error = new HashMap<>();
                    error.put(err.getField(), err.getDefaultMessage());
                    return error;
                }
        ).collect(Collectors.toList());Collectors.toList();

        ErrorMessage errorMessage = ErrorMessage.builder()
                .code("01")
                .messages(messages).build();

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }
}
