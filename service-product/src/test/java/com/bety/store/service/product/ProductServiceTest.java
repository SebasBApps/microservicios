package com.bety.store.service.product;

import com.bety.store.service.product.entity.Category;
import com.bety.store.service.product.entity.Product;
import com.bety.store.service.product.repository.IProductRepository;
import com.bety.store.service.product.service.ProductService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
public class ProductServiceTest {

    @Mock
    private IProductRepository productRepository;

    private ProductService productService;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        productService = new ProductService(productRepository);
        Product computer = Product.builder()
                .id(1L)
                .name("Computer")
                .category(Category.builder().id(1L).build())
                .price(Double.parseDouble("1235.16"))
                .stock(Double.parseDouble("133"))
                .build();

        Mockito.when(productRepository.findById(1L)).thenReturn(Optional.of(computer));
        Mockito.when(productRepository.save(computer)).thenReturn(computer);
    }

    @Test
    public void whenValidGetId_then_returnProduct() {
        Product product = productService.getProduct(1L);
        Assertions.assertThat(product.getName()).isEqualTo("Computer");
    }

    @Test
    public void whenValidUpadteStock_then_returnNewStock() {
        Product product = productService.updateStock(1L, Double.parseDouble("7"));
        Assertions.assertThat(product.getStock()).isEqualTo(140);
    }

}
