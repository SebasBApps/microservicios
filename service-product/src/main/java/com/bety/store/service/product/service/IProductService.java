package com.bety.store.service.product.service;

import com.bety.store.service.product.entity.Category;
import com.bety.store.service.product.entity.Product;

import java.util.List;

public interface IProductService {
    List<Product> listAllProduct();

    Product getProduct(Long id);

    Product createProduct(Product product);

    Product updateProduct(Product product);

    Product deleteProduct(Long id);

    List<Product> findByCategory(Category category);

    Product updateStock(Long id, Double quantity);


}
