package com.bety.store.service.product.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

@Entity
@Table(name = "products")
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "EL nombre no debe ser vacío")
    private String name;
    private String description;
    private Double price;
    @Positive(message = "El stock debe ser mayor a cero")
    private Double stock;
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @NotNull(message = "La categoría no puede esta vacía")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_category")
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    private Category category;

}
