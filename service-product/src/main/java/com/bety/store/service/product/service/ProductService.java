package com.bety.store.service.product.service;

import com.bety.store.service.product.entity.Category;
import com.bety.store.service.product.entity.Product;
import com.bety.store.service.product.repository.IProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService implements IProductService{

    private final IProductRepository productRepository;

    @Override
    public List<Product> listAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product getProduct(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Product createProduct(Product product) {
        product.setStatus("CREATED");
        product.setCreationDate(new Date());
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(Product product) {
        Product productFound = getProduct(product.getId());
        if (productFound == null)
            return null;

        productFound.setName(product.getName());
        productFound.setDescription(product.getDescription());
        productFound.setCategory(product.getCategory());
        productFound.setPrice(product.getPrice());
        productFound.setStock(product.getStock());
        return productRepository.save(productFound);
    }

    @Override
    public Product deleteProduct(Long id) {
        Product productFound = getProduct(id);
        if (productFound == null)
            return null;

        productFound.setStatus("DELETED");
        return productRepository.save(productFound);
    }

    @Override
    public List<Product> findByCategory(Category category) {
        return productRepository.findByCategory(category);
    }

    @Override
    public Product updateStock(Long id, Double quantity) {
        Product productFound = getProduct(id);
        if (productFound == null)
            return null;

        productFound.setStock(productFound.getStock() + quantity);
        return productRepository.save(productFound);
    }
}
